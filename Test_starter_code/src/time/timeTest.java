/**
 * 
 */
package time;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.lang.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author nupur adhvaryu
 */
public class timeTest {

//	@Test
//	public void testGetTotalMilliseconds() {
//		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
//		assertTrue("length of milli sec", totalMilliseconds == 5);
//	}


//	@Test (expected = NumberFormatException.class)
//	public void testGetTotalSecondsException() {
//		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0S");
//		fail("invalid formation");
//	}
//	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("invalid number of milli sec", totalMilliseconds == 999);
	}
	
//	
//	@Test 
//	public void testGetTotalSecondsBoundaryOut() {
//		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:1000");
//		assertTrue("invalid number of milli sec", totalMilliseconds != 1000);
//	}

	
}
